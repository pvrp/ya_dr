package main

import (
	"errors"
	"log"
	"net/smtp"
)

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unkown fromServer")
		}
	}
	return nil, nil
}

func SendMail(to, link string) {

	auth := LoginAuth(Config.MailUser,Config.MailPassword)

	receiver := []string{to}
	// Here we do it all: connect to our server, set up a message and send it
	msg := []byte("To: " + to +"\r\n" +
		"Subject: Your password reset link for golang auth\r\n" +
		"\r\n" +
		link)

	err := smtp.SendMail("smtp.gmail.com:587", auth, Config.MailUser, receiver, msg)
	if err != nil {
		log.Fatal(err)
	}
}