# Golang Auth

The following things are not implemented:

- Errors and success messages
- Tests

Apart from that everything should work just fine.

The database tables will be created automatically during startup.

To run the code you need to create a `config` directory and place a `google_secret.json` inside.

`google_secret.json` should have the following format:

```json
{
    "client_id": "google client id for oauth",
    "client_secret": "google client secret for oauth",
    "redirect_url": "google callback url for oauth",
    "session_secret": "session secret for the goth session",
    "base_url": "your apps base url e.g. https://golangauth.herokuapp.com/",

    "mail_user": "gmail account username",
    "mail_password": "gmail account password",
    "db_name": "database name",
    "db_user": "database user",
    "db_password": "database password",
    "db_host": "database host domain"
}
```