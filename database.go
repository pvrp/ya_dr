package main

import (
	"database/sql"
	"fmt"
	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

var DB *sql.DB

func InitDb() {
	var err error

	//mysql://b90364eba40193:d71783ef@us-cdbr-east-05.cleardb.net/heroku_32f96afc2c67455?reconnect=true
	DB, err = sql.Open("mysql", Config.DatabaseUser +  ":" + Config.DatabasePassword + "@(" + Config.DatabaseHost + ":3306)/" + Config.DatabaseName)

	if err != nil {
		log.Fatal(err)
	}

	_, err = DB.Exec("CREATE TABLE IF NOT EXISTS users(id INT PRIMARY KEY AUTO_INCREMENT, authType VARCHAR(64), email VARCHAR(255) NOT NULL UNIQUE, fullname VARCHAR(255) NOT NULL, telephone VARCHAR(128), address VARCHAR(255));")
	if err != nil {
		log.Fatal(err)
	}

	_, err = DB.Exec("CREATE TABLE IF NOT EXISTS password(id INT PRIMARY KEY AUTO_INCREMENT, passwordHash VARCHAR(255) NOT NULL, userId INT, FOREIGN KEY (userId) REFERENCES users(id));")
	if err != nil {
		log.Fatal(err)
	}

	_, err = DB.Exec("CREATE TABLE IF NOT EXISTS oauth(id INT PRIMARY KEY AUTO_INCREMENT, googleId VARCHAR(255) NOT NULL UNIQUE, userId INT, FOREIGN KEY (userId) REFERENCES users(id));")
	if err != nil {
		log.Fatal(err)
	}

	_, err = DB.Exec("CREATE TABLE IF NOT EXISTS session(id INT PRIMARY KEY AUTO_INCREMENT, session VARCHAR(255) NOT NULL UNIQUE, userId INT UNIQUE, FOREIGN KEY (userId) REFERENCES users(id));")
	if err != nil {
		log.Fatal(err)
	}

	_, err = DB.Exec("CREATE TABLE IF NOT EXISTS reset(id INT PRIMARY KEY AUTO_INCREMENT, hash VARCHAR(255) NOT NULL UNIQUE, email VARCHAR(255) NOT NULL UNIQUE, timestamp datetime default CURRENT_TIMESTAMP);")
	if err != nil {
		log.Fatal(err)
	}

	// Delete reset password links older than 15 minutes every minute
	go func() {
		for {
			_, err := DB.Exec("SELECT * from reset WHERE timestamp < NOW() - INTERVAL 15 MINUTE")

			if err != nil {
				log.Println(err)
			}

			time.Sleep(time.Minute)
		}
	}()

	fmt.Println("Database successfully connected and tables created (if not exists)")
}

type User struct {
	Id             int64
	AuthType       AuthType
	FullName       string
	Email          string
	Telephone      string
	Address        string
	Authentication string
}

type AuthType string

const (
	OAuth    AuthType = "oauth"
	Password AuthType = "password"
)

func (u *User) insertUser() (int64, error) {
	res, err := DB.Exec("INSERT INTO users(authType, email, fullname, telephone, address) VALUES(?, ?, ?, ?, ?)", u.AuthType, u.Email, u.FullName, u.Telephone, u.Address)

	if err != nil {
		return -1, err
	}

	userId, err := res.LastInsertId()

	if err != nil {
		return -1, err
	}

	if u.AuthType == OAuth {
		_, err := DB.Exec("INSERT INTO oauth(googleId, userId) VALUES(?, ?)", u.Authentication, userId)
		if err != nil {
			return -1, err
		}
	} else if u.AuthType == Password {
		_, err := DB.Exec("INSERT INTO password(passwordHash, userId) VALUES(?, ?)", u.Authentication, userId)
		if err != nil {
			return -1, err
		}
	}

	return userId, err
}

func GetUserByEmail(email string) (*User, error) {
	var user User

	err := DB.QueryRow("SELECT id, authType, email, fullname, telephone, address FROM users WHERE email = ?;", email).Scan(&user.Id, &user.AuthType, &user.Email, &user.FullName, &user.Telephone, &user.Address)

	if err != nil {
		return nil, err
	}

	if user.AuthType == Password {
		err := DB.QueryRow("SELECT passwordHash FROM password WHERE userId = ?;", user.Id).Scan(&user.Authentication)

		if err != nil {
			return nil, err
		}
	}

	return &user, err
}

func (u *User) createSession() (string, error) {
	session := RandomString(64)

	_, err := DB.Exec("INSERT INTO session(session, userId) VALUES(?, ?)", session, u.Id)

	if driverErr, ok := err.(*mysql.MySQLError); ok {
		if driverErr.Number == mysqlerr.ER_DUP_ENTRY {

			err = u.deleteSession()

			if err != nil {
				return "", err
			}

			_, err = DB.Exec("INSERT INTO session(session, userId) VALUES(?, ?)", session, u.Id)

			if err != nil {
				return "", err
			}
		}
	}

	return session, nil
}

func (u *User) deleteSession() error {
	_, err := DB.Exec("DELETE FROM session WHERE userId = ?", u.Id)
	return err
}

func GetUserBySession(sessionCookie string) (*User, error) {
	var user User

	err := DB.QueryRow("SELECT users.id, authType, email, fullname, telephone, address FROM users JOIN session ON users.id=session.userId WHERE session = ?;", sessionCookie).Scan(&user.Id, &user.AuthType, &user.Email, &user.FullName, &user.Telephone, &user.Address)

	if err != nil {
		return nil, err
	}

	return &user, err
}

func (u *User) updateUserDetails() error {
	_, err := DB.Exec("UPDATE users SET email = ?, fullname = ?, telephone = ?, address = ? WHERE id = ?", u.Email, u.FullName, u.Telephone, u.Address, u.Id)
	return err
}

func (u *User) updateUserPassword() error {
	_, err := DB.Exec("UPDATE password SET passwordHash = ? WHERE userId = ?", u.Authentication, u.Id)
	return err
}

func CreateResetLinkForEmail(email string) (string, error) {
	hash := RandomString(64)

	_, err := DB.Exec("INSERT INTO reset(hash, email) VALUES(?, ?)", hash, email)

	return hash, err
}

func GetUserFromResetHash(hash string) (*User, error) {
	var email string

	err := DB.QueryRow("SELECT email FROM reset WHERE hash = ?;", hash).Scan(&email)

	if err != nil {
		return nil, err
	}

	user, err := GetUserByEmail(email)

	if err != nil {
		return nil, err
	}

	return user, err
}

func DeleteHash(hash string) error {
	_, err := DB.Exec("DELETE FROM reset WHERE hash = ?", hash)
	return err
}
