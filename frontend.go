package main

import (
	"github.com/markbates/goth/gothic"
	"html/template"
	"net/http"
)

func init() {
	WebRouter.Get("/profile", func(res http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("session")

		if err != nil {
			// User not logged in
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user, err := GetUserBySession(cookie.Value)

		if err != nil {
			// Cookie not in database -> invalid session
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		res.Header().Add("Cache-Control", "no-store")

		t, _ := template.ParseFiles("templates/success.html")
		t.Execute(res, user)
	})

	WebRouter.Get("/register", func(res http.ResponseWriter, req *http.Request) {
		t, _ := template.ParseFiles("templates/register-email.html")
		t.Execute(res, nil)
	})

	WebRouter.Get("/auth/password", func(res http.ResponseWriter, req *http.Request) {
		t, _ := template.ParseFiles("templates/login-email.html")
		t.Execute(res, nil)
	})

	WebRouter.Get("/edit", func(res http.ResponseWriter, req *http.Request) {
		t, _ := template.ParseFiles("templates/edit.html")

		cookie, err := req.Cookie("session")

		if err != nil {
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user, err := GetUserBySession(cookie.Value)

		if err != nil {

			http.SetCookie(res, &http.Cookie{
				Name:     "session",
				Value:    "",
				Path:     "/",
				MaxAge:   -1,
				HttpOnly: true,
			})

			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		t.Execute(res, user)
	})

	WebRouter.Get("/auth/{provider}", func(res http.ResponseWriter, req *http.Request) {
		gothic.BeginAuthHandler(res, req)
	})

	WebRouter.Get("/reset/{hash}", func(res http.ResponseWriter, req *http.Request) {
		t, _ := template.ParseFiles("templates/password-reset.html")

		hash := req.URL.Query().Get(":hash")

		t.Execute(res, struct {
			Hash string
		}{hash})
	})

	WebRouter.Get("/reset", func(res http.ResponseWriter, req *http.Request) {
		t, _ := template.ParseFiles("templates/send-reset.html")
		t.Execute(res, nil)
	})

	WebRouter.Get("/", func(res http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("session")

		if err != nil {
			t, _ := template.ParseFiles("templates/index.html")
			t.Execute(res, false)
			return
		}

		_, err = GetUserBySession(cookie.Value)

		if err != nil {
			http.SetCookie(res, &http.Cookie{
				Name:     "session",
				Value:    "",
				Path:     "/",
				MaxAge:   -1,
				HttpOnly: true,
			})

			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		http.Redirect(res, req, "/profile", http.StatusFound)
	})
}