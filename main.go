package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/pat"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

var WebRouter = pat.New()
var Config JsonConfig

type JsonConfig struct {
	ClientId string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectUrl string `json:"redirect_url"`
	SessionSecret string `json:"session_secret"`
	BaseUrl string `json:"base_url"`

	MailUser string `json:"mail_user"`
	MailPassword string `json:"mail_password"`
	DatabaseName string `json:"db_name"`
	DatabaseUser string `json:"db_user"`
	DatabasePassword string `json:"db_password"`
	DatabaseHost string `json:"db_host"`
}

func init() {
	jsonFile, err := os.Open("config/google_secret.json")

	if err != nil {
		fmt.Println(err)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &Config)
}

func main() {

	key := Config.SessionSecret // Replace with your SESSION_SECRET or similar
	maxAge := 86400 * 30        // 30 days
	isProd := false             // Set to true when serving over https

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true // HttpOnly should always be enabled
	store.Options.Secure = isProd

	gothic.Store = store

	goth.UseProviders(
		google.New(Config.ClientId, Config.ClientSecret, Config.RedirectUrl, "email", "profile"),
	)

	InitDb()

	port := os.Getenv("PORT")

	log.Println("listening on localhost:8080")
	log.Fatal(http.ListenAndServe(":" + port, WebRouter))
}
