package main

import (
	"github.com/markbates/goth/gothic"
	"golang.org/x/crypto/bcrypt"
	"html/template"
	"net/http"
)

func init() {
	WebRouter.Get("/auth/{provider}/callback", func(res http.ResponseWriter, req *http.Request) {
		user, err := gothic.CompleteUserAuth(res, req)

		if err != nil {
			http.Redirect(res, req, "/", http.StatusPermanentRedirect)
			return
		}

		dbUser := &User{
			AuthType:       OAuth,
			FullName:       user.Name,
			Email:          user.Email,
			Authentication: user.UserID,
			Telephone:      "",
			Address:        "",
		}

		_, err = dbUser.insertUser()

		if err != nil {
			// This means the user already exists
			dbUser, err = GetUserByEmail(dbUser.Email)

			if err != nil {
				http.Redirect(res, req, "/", http.StatusPermanentRedirect)
				return
			}

			sessionCookie, err := dbUser.createSession()

			if err != nil {
				http.Redirect(res, req, "/", http.StatusPermanentRedirect)
				return
			}

			http.SetCookie(res, &http.Cookie{
				Name:     "session",
				Value:    sessionCookie,
				Path:     "/",
				HttpOnly: true,
			})

			http.Redirect(res, req, "/profile", http.StatusPermanentRedirect)
			return
		}

		dbUser, err = GetUserByEmail(dbUser.Email)

		if err != nil {
			http.Redirect(res, req, "/", http.StatusPermanentRedirect)
			return
		}

		sessionCookie, err := dbUser.createSession()

		if err != nil {
			http.Redirect(res, req, "/", http.StatusPermanentRedirect)
			return
		}

		http.SetCookie(res, &http.Cookie{
			Name:     "session",
			Value:    sessionCookie,
			Path:     "/",
			HttpOnly: true,
		})

		http.Redirect(res, req, "/edit", http.StatusFound)
	})

	WebRouter.Post("/register", func(res http.ResponseWriter, req *http.Request) {
		req.ParseForm()

		t, _ := template.ParseFiles("templates/register-email.html")

		if req.FormValue("password") != req.FormValue("passwordRepeat") {
			t.Execute(res, nil)
			return
		}

		passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.FormValue("password")), 14)

		if err != nil {
			t.Execute(res, nil)
			return
		}

		dbUser := &User{
			AuthType:       Password,
			FullName:       "",
			Email:          req.FormValue("email"),
			Authentication: string(passwordHash),
			Telephone:      "",
			Address:        "",
		}

		_, err = dbUser.insertUser()

		if err != nil {
			t.Execute(res, nil)
			return
		}

		user, err := GetUserByEmail(req.FormValue("email"))

		if err != nil {
			t.Execute(res, nil)
			return
		}

		sessionCookie, err := user.createSession()

		if err != nil {
			t.Execute(res, nil)
			return
		}

		http.SetCookie(res, &http.Cookie{
			Name:     "session",
			Value:    sessionCookie,
			Path:     "/",
			HttpOnly: true,
		})

		http.Redirect(res, req, "/edit", http.StatusFound)
	})

	WebRouter.Post("/auth/password", func(res http.ResponseWriter, req *http.Request) {
		req.ParseForm()

		user, err := GetUserByEmail(req.FormValue("email"))

		t, _ := template.ParseFiles("templates/login-email.html")

		if err != nil {
			t.Execute(res, nil)
			return
		}

		err = bcrypt.CompareHashAndPassword([]byte(user.Authentication), []byte(req.FormValue("password")))

		if err != nil {
			t.Execute(res, nil)
			return
		}

		sessionCookie, err := user.createSession()

		if err != nil {
			t.Execute(res, nil)
			return
		}

		http.SetCookie(res, &http.Cookie{
			Name:     "session",
			Value:    sessionCookie,
			Path:     "/",
			HttpOnly: true,
		})

		http.Redirect(res, req, "/profile", http.StatusFound)
	})

	WebRouter.Post("/logout", func(res http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("session")

		if err != nil {
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user, err := GetUserBySession(cookie.Value)

		if err != nil {
			// Cookie not in database -> invalid session
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		err = user.deleteSession()

		if err != nil {
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		http.SetCookie(res, &http.Cookie{
			Name:     "session",
			Value:    "",
			Path:     "/",
			MaxAge:   -1,
			HttpOnly: true,
		})

		http.Redirect(res, req, "/", http.StatusFound)
	})

	WebRouter.Post("/edit", func(res http.ResponseWriter, req *http.Request) {
		req.ParseForm()

		cookie, err := req.Cookie("session")

		if err != nil {
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user, err := GetUserBySession(cookie.Value)

		if err != nil {
			http.SetCookie(res, &http.Cookie{
				Name:     "session",
				Value:    "",
				Path:     "/",
				MaxAge:   -1,
				HttpOnly: true,
			})

			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user.Address = req.FormValue("address")
		user.Telephone = req.FormValue("telephone")
		user.FullName = req.FormValue("fullName")

		if user.AuthType != OAuth {
			user.Email = req.FormValue("email")
		}

		err = user.updateUserDetails()

		if err != nil {
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		http.Redirect(res, req, "/profile", http.StatusFound)
	})

	WebRouter.Post("/reset/{hash}", func(res http.ResponseWriter, req *http.Request) {
		hash := req.URL.Query().Get(":hash")

		user, err := GetUserFromResetHash(hash)

		if err != nil {
			DeleteHash(hash)
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		req.ParseForm()

		if req.FormValue("password") != req.FormValue("repeatPassword") {
			// Error passwords do not match
			t, _ := template.ParseFiles("templates/password-reset.html")
			t.Execute(res, struct {
				Hash string
			}{hash})

			return
		}

		passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.FormValue("password")), 14)

		if err != nil {
			// Server error
			http.Redirect(res, req, "/", http.StatusFound)
			return
		}

		user.Authentication = string(passwordHash)
		user.updateUserPassword()
		DeleteHash(hash)

		http.Redirect(res, req, "/auth/password", http.StatusFound)
	})

	WebRouter.Post("/reset", func(res http.ResponseWriter, req *http.Request) {
		req.ParseForm()

		user, err := GetUserByEmail(req.FormValue("email"))

		if err != nil || user.AuthType != Password {
			// Do not send mail if user with email doesn't exist
			// or if user is signed up with OAuth
			t, _ := template.ParseFiles("templates/send-reset.html")
			t.Execute(res, nil)
			return
		}

		hash, err := CreateResetLinkForEmail(req.FormValue("email"))

		if err != nil {
			t, _ := template.ParseFiles("templates/send-reset.html")
			t.Execute(res, nil)
			return
		}

		SendMail(req.FormValue("email"), Config.BaseUrl + "/reset/" + hash)

		http.Redirect(res, req, "/", http.StatusFound)
	})
}
